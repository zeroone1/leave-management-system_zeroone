package com.hexaware.lms.service;
import java.time.LocalDate;
import org.junit.Before;
import com.hexaware.lms.dao.LeaveDetailsDAO;
import com.hexaware.lms.dao.LeaveDetailsDAOImpl;
import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.model.LeaveStatus;
import static org.easymock.EasyMock.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.time.LocalDate;
import static org.junit.Assert.*;

import java.time.LocalDate;


import org.junit.Before;
import org.junit.Test;

import com.hexaware.lms.dao.LeaveDetailsDAO;
import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;


import static org.easymock.EasyMock.*;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.*;



public class LeaveDetailsServiceTest {
<<<<<<< HEAD
	LeaveDetails leaveDetails = null;
    LeaveDetailsDAO leaveDetailsDAO;
    LeaveDetailsService leaveDetailsService;
    Employee employee;

    @Before
    public void initializeLeaveDetails(){
        leaveDetails = new LeaveDetails(15,LocalDate.now(),LocalDate.of(2019,12,12));
        leaveDetailsDAO = createMock(LeaveDetailsDAO.class);
        leaveDetailsService = new LeaveDetailsServiceImpl(leaveDetailsDAO);
        employee= new Employee(12,"Bassi","Bassi@gmail.com",LocalDate.of(2019,12,12));
    }
   
    
   @Test
   public void testLeaveBalanceWithValidEmployeeId(){

   	employee.setLeaveBalance(50);
   	leaveDetails.setStartDate(LocalDate.now());
   	leaveDetails.setEndDate(LocalDate.of(2020, 3,15));
   	expect(leaveDetailsDAO.findByEmployeeId(12)).andReturn(employee);
	expect(leaveDetailsDAO.save(12, leaveDetails)).andReturn(leaveDetails);
   	replay(leaveDetailsDAO);
   	try {
   	    leaveDetailsService.getLeaveBalance(12);
   	    fail("Should throw insufficient leave balance");
   	} catch (Exception e){
   	   assertNotNull(e);
   	   assertTrue(e instanceof IllegalArgumentException );
   	   assertEquals(e.getMessage(),"Employee does not exist");

   	}
   }
   
   @Test
   public void testLeaveBalanceWithInvalidEmployeeId(){
   	expect(leaveDetailsDAO.findByEmployeeId(15)).andReturn(null);
   	 replay(leaveDetailsDAO);
   	 try{
   		 leaveDetailsService.getLeaveBalance(15);
   		 fail("should throw an exception");
   	 } catch(Exception e){
   		 assertNotNull(e);
   		 assertTrue( e instanceof IllegalArgumentException);
   		 assertEquals(e.getMessage(),"Employee does not exist");
   	 }
   	
   } 
   

   	

=======
	
	LeaveDetails leaveDetails = null;
    LeaveDetailsDAO leaveDetailsDAO;
    LeaveDetailsService leaveDetailsService;
>>>>>>> fb5090bb0cc70a633b1a0c30c41b3710f927838f


	@Before
	public void setUp() {
		leaveDetails = new LeaveDetails(15,LocalDate.now(),LocalDate.of(2020, 6, 10));
		leaveDetailsDAO =createMock(LeaveDetailsDAO.class);
        leaveDetailsService = new LeaveDetailsServiceImpl(leaveDetailsDAO);
	}
		
	@Test
    public void testApplyForLeaveWithInvalidEmpID() {
       expect(leaveDetailsDAO.findByEmployeeId(15)).andReturn(null);
        replay(leaveDetailsDAO);
        try {
            leaveDetailsService.applyForLeave(15, leaveDetails);
            fail("Should throw an exception");
        } catch (Exception e){
            assertNotNull(e);
            assertTrue( e instanceof IllegalArgumentException);
            assertEquals(e.getMessage(), "Employee does not exist");
        }
    }
	@Test
    public void testApplyForLeaveWithValidEmpID() {
        Employee employee = new Employee(15, "Aman", "aman@gmail.com", LocalDate.of(2011, 9, 1));
        employee.setLeaveBalance(50);
        leaveDetails.setStartDate(LocalDate.now());
        leaveDetails.setEndDate(LocalDate.of(2020,9,15));
        expect(leaveDetailsDAO.findByEmployeeId(15)).andReturn(employee);
        expect(leaveDetailsDAO.save(15,leaveDetails)).andReturn(leaveDetails);
        replay(leaveDetailsDAO);
        try {
            leaveDetailsService.applyForLeave(15,leaveDetails );
            fail("Exception must be thrown for insufficient balance");
        } catch (Exception e){
           assertNotNull(e);
           assertTrue( e instanceof IllegalArgumentException);
           assertEquals(e.getMessage(), "Leave Balance Insufficient");

        }
    }
	 @Test
	    public void testApplyForLeaveWithValidEmpIDAndValidDays() {
	        Employee employee = new Employee(1, "Shweta", "s@gmail.com", LocalDate.of(2020, 01, 15));
	        employee.setLeaveBalance(50);
	        leaveDetails.setStartDate(LocalDate.now());
	        leaveDetails.setEndDate(LocalDate.of(2020, 3,15));
	        expect(leaveDetailsDAO.findByEmployeeId(12)).andReturn(employee);
	        LeaveDetails leaveDetails2 = new LeaveDetails(12, LocalDate.of(2020, 3, 12),LocalDate.of(2020, 3, 13));
	        leaveDetails2.setLeaveReason("Updating the status");
	        leaveDetails2.setLeaveStatus(LeaveStatus.PENDING);
	        expect(leaveDetailsDAO.save(12, leaveDetails)).andReturn(leaveDetails2);
	        replay(leaveDetailsDAO);
	        try {
	            LeaveDetails returnValue = leaveDetailsService.applyForLeave(12, leaveDetails);
	            Assert.assertNotNull(returnValue);
	            assertEquals(returnValue.getLeaveStatus(), LeaveStatus.PENDING);
	        } catch (Exception e){
	            fail("SHould throw insufficient leave balance");
	        }
	    }
}
   
    	
   	
  

 





