package lmscom.hexaware.lms.model;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.model.LeaveStatus;


public class LeaveDetailsTesting {
	LeaveDetails leaveDetails;
	Employee employee;
	
	@Before
	public void setUp() {
		leaveDetails = new LeaveDetails(1, LocalDate.of(2019, 2, 11), LocalDate.of(2019, 2, 15));
		employee = new Employee(12, "Aman", "aman@gmail.com", LocalDate.of(2011, 6, 1));
	}
		
	@Test
	public void testLeaveDetailsConstructor(){
		
		assertNotNull(leaveDetails);
		assertEquals(leaveDetails.getLeaveId(),1);
		assertEquals(leaveDetails.getStartDate(),LocalDate.of(2019, 2, 11));
		assertEquals(leaveDetails.getEndDate(),LocalDate.of(2019, 2, 15));
		
	}
	
	@Test
	public void testLeaveDetailsSetLeaveId() {
		leaveDetails.setLeaveId(1200);
		assertEquals(1200,leaveDetails.getLeaveId());
		assertNotEquals(1100,leaveDetails.getLeaveId());
		
	}
	@Test
	public void testLeaveDetailsSetStartDate() {
		leaveDetails.setStartDate(LocalDate.of(2019, 2, 11));
		assertEquals(LocalDate.of(2019, 2, 11),leaveDetails.getStartDate());
		assertNotEquals(LocalDate.of(2010, 2, 11),leaveDetails.getStartDate());
		
	}
	@Test
	public void testLeaveDetailsSetEndDate() {
		leaveDetails.setEndDate(LocalDate.of(2019, 2, 15));
		assertEquals(LocalDate.of(2019, 2, 15),leaveDetails.getEndDate());
		assertNotEquals(LocalDate.of(2010, 2, 15),leaveDetails.getEndDate());
		
	}
	
	@Test
	public void testLeaveDetailsSetLeaveReason() {
		leaveDetails.setLeaveReason("fever");
		assertEquals("fever",leaveDetails.getLeaveReason());
		assertNotEquals("holi",leaveDetails.getLeaveReason());
		
	}
	
	@Test
	public void testLeaveDetailsSetLeaveStatus() {
		leaveDetails.setLeaveStatus(LeaveStatus.PENDING);
		assertEquals(LeaveStatus.PENDING,leaveDetails.getLeaveStatus());
		assertNotEquals(LeaveStatus.APPROVED,leaveDetails.getLeaveStatus());
	}
	
	@Test
	public void testLeaveDetailsSetComments() {
		leaveDetails.setComments("Leave Application is approved");
		assertEquals("Leave Application is approved",leaveDetails.getComments());
		assertNotEquals("Leave Application is not approved",leaveDetails.getComments());
	}
	
	@Test
	public void testLeaveDetailsLeaveSetEmployee() {
		leaveDetails.setEmployee(employee);
		assertEquals(employee,leaveDetails.getEmployee());
	}
	
	@Test
	public void testEquals() {
		LeaveDetails leaveDetails1=new LeaveDetails(1, LocalDate.of(2019, 2, 11), LocalDate.of(2019, 2, 15));
		LeaveDetails leaveDetails2=new LeaveDetails(10, LocalDate.of(2019, 2, 11), LocalDate.of(2019, 2, 15));
		assertTrue(leaveDetails.equals(leaveDetails1));
		assertFalse(leaveDetails.equals(leaveDetails2));
	}
	
	@Test
	public void testHashCodeEqual() {
		LeaveDetails leaveDetails1=new LeaveDetails(1, LocalDate.of(2019, 2, 11), LocalDate.of(2019, 2, 15));
		assertEquals(leaveDetails1,leaveDetails);
	}
	@Test
	public void testHashCodeNotEqual() {
		LeaveDetails leaveDetails1=new LeaveDetails(2, LocalDate.of(2019, 2, 11), LocalDate.of(2019, 2, 15));
		assertNotEquals(leaveDetails1,leaveDetails);
	}
	@Test
	public void testToStringEquals() {
		assertEquals("LeaveDetails [leaveId=1, startDate=2019-02-11, endDate=2019-02-15, leaveReason=null, leaveStatus=null, comments=null]",leaveDetails.toString());
	}
	@Test
	public void testToStringNotEquals() {
		assertNotEquals("LeaveDetails [leaveId=12, startDate=2019-02-11, endDate=2019-02-15, leaveReason=null, leaveStatus=null, comments=null]",leaveDetails.toString());
	}
	
}
