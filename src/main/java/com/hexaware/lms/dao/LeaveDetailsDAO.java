package com.hexaware.lms.dao;


import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;


public interface LeaveDetailsDAO {
	
	public LeaveDetails save(long empId, LeaveDetails leaveDetails);
	
	public LeaveDetails findByLeaveId(long leaveId);
	
	public Employee findByEmployeeId(long empId);
	
	public LeaveDetails update(long leaveId, LeaveDetails leaveDetails);

}
