package com.hexaware.lms.service;

import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.model.LeaveStatus;

public interface LeaveDetailsService {

	public LeaveDetails applyForLeave(long empId, LeaveDetails leaveDetails);

	public LeaveDetails approveOrDeny(long leaveId, long managerId, LeaveStatus leavestatus, String comments);

	public int getLeaveBalance(long employeeId);
	

}
