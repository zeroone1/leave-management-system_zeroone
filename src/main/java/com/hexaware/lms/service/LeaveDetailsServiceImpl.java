package com.hexaware.lms.service;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

import com.hexaware.lms.dao.LeaveDetailsDAO;
import com.hexaware.lms.dao.LeaveDetailsDAOImpl;
import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.model.LeaveStatus;

public class LeaveDetailsServiceImpl implements LeaveDetailsService {
	LeaveDetailsDAO leaveDetailsDAO;
	Employee employee;

<<<<<<< HEAD

	public LeaveDetailsServiceImpl(LeaveDetailsDAO leaveDetailsDAO2) {
		// TODO Auto-generated constructor stub
=======
	public LeaveDetailsServiceImpl(LeaveDetailsDAO leaveDetailsDAO) {
		this.leaveDetailsDAO = leaveDetailsDAO;
>>>>>>> fb5090bb0cc70a633b1a0c30c41b3710f927838f
	}

	public LeaveDetails applyForLeave(long empId, LeaveDetails leaveDetails) {

		employee = leaveDetailsDAO.findByEmployeeId(empId);

		if (employee == null) {
			throw new IllegalArgumentException("Employee does not exist");
		} else {

			LocalDate startDate = leaveDetails.getStartDate();
			LocalDate endDate = leaveDetails.getEndDate();
			long appliedNoOfDays = ChronoUnit.DAYS.between(startDate, endDate);
			System.out.println(endDate);
			if (employee.getLeaveBalance() < appliedNoOfDays+1) {
				throw new IllegalArgumentException("Leave Balance Insufficient");
			} else {
				leaveDetailsDAO.save(empId, leaveDetails);
				System.out.println(leaveDetails);
			}
		}
		return leaveDetails;
	}

	public int getLeaveBalance(long employeeId) {

		employee = leaveDetailsDAO.findByEmployeeId(employeeId);
		if (employee == null) {

			throw new IllegalArgumentException("Employee does not exist");
		} else {
			return employee.getLeaveBalance();
		}
	}

	public LeaveDetails approveOrDeny(long leaveId, long managerId, LeaveStatus leaveStatus, String comments) {

		LeaveDetails leaveDetails = leaveDetailsDAO.findByLeaveId(leaveId);

		if (leaveDetails == null) {
			throw new IllegalArgumentException("Leave Details does not exist");
		} else {
			Employee employee = leaveDetails.getEmployee();
			long manId = employee.getManager().getEmpId();

			if (manId != managerId) {
				throw new IllegalArgumentException("You are not the manager for this employee");
			} else {
				leaveDetails.setLeaveStatus(leaveStatus);
				leaveDetails.setComments(comments);
				leaveDetailsDAO.update(leaveId, leaveDetails);
			}
		}

		return leaveDetails;
	}

}
