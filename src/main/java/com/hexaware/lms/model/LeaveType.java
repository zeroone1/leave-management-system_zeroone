package com.hexaware.lms.model;

public enum LeaveType {
	 
	OPTIONAL,
	 
	 SICK,
	
	 CASUAL;
}

