package com.hexaware.lms.model;

import java.time.LocalDate;
import java.util.Date;

public class LeaveDetails {
	
	private long leaveId;
	private LocalDate startDate,endDate;
	private String leaveReason;
	private LeaveStatus leaveStatus;
	private String comments;
	private Employee employee;
	
	
	public LeaveDetails(long leaveId, LocalDate startDate, LocalDate endDate) {
		super();
		this.leaveId = leaveId;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	
	public long getLeaveId() {
		return leaveId;
	}


	public void setLeaveId(long leaveId) {
		this.leaveId = leaveId;
	}


	public LocalDate getStartDate() {
		return startDate;
	}


	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}


	public LocalDate getEndDate() {
		return endDate;
	}


	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}


	public String getLeaveReason() {
		return leaveReason;
	}


	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}


	public LeaveStatus getLeaveStatus() {
		return leaveStatus.PENDING;
	}


	public void setLeaveStatus(LeaveStatus leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Employee getEmployee() {
		return employee;
	}


	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	@Override
	public String toString() {
		return "LeaveDetails [leaveId=" + leaveId + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", leaveReason=" + leaveReason + ", leaveStatus=" + leaveStatus + ", comments=" + comments + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + (int) (leaveId ^ (leaveId >>> 32));
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LeaveDetails other = (LeaveDetails) obj;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (leaveId != other.leaveId)
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}

}
