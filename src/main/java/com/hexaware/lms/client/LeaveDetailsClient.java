package com.hexaware.lms.client;

import java.time.LocalDate;

import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.service.LeaveDetailsService;
import com.hexaware.lms.service.LeaveDetailsServiceImpl;

public class LeaveDetailsClient {
	public static void main(String args[]) {
		
		LeaveDetails leaveDetails= new LeaveDetails(1, LocalDate.of(2019, 2, 11), LocalDate.of(2019, 2, 15));
		LeaveDetailsService leaveDetailsService = new LeaveDetailsServiceImpl(null);
		leaveDetails = leaveDetailsService.applyForLeave(1200, leaveDetails);
		
	}

}
